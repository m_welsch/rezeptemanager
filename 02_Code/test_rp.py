from Recipe_Processor import *

RP = Recipe_Processor()

r1 = {'Name':'Müsli', 'Kategorien':'Frühstück', 'Personenzahl':'2', 'Anweisungen':'einfach machen', 'Zutaten':[['1','EL','Mais']]}
r2 = {'Name':'Müsli', 'Kategorien':'Frühstück', 'Personenzahl':'2', 'Anweisungen':'  einfach machen', 'Zutaten':[['1','EL','Mais']]}
r3 = {'Name':'Müsli2', 'Kategorien':'Frühstück2', 'Personenzahl':'2', 'Anweisungen':'einfach machen', 'Zutaten':[['2','EL','Mais']]}

print("r1 and r2 identical = %r" % RP.recipes_identical(r1,r2))
print("r1 and r3 identical = %r" % RP.recipes_identical(r1,r3))
