import sqlite3
from difflib import SequenceMatcher

# Trägt Neue Zutat ein.
# Gibt 0 zurück, falls erfolgreich
# Gibt einen Zutatennamen zurück, falls Zutat mit großer Ähnlichkeit gefunden wurde.
def Neue_Zutat_Eintragen(Neue_Zutat):
	#Neue_Zutat = input("Bitte Neue Zutat eingeben: ")
	Alle_Zutaten = c.execute('''SELECT Zutaten_Name FROM Zutaten''')

	for Zutat in Alle_Zutaten:
		Zutat = str(Zutat)[2:-3] #Anfuehrungszeichen entfernen
		Aehnlichkeit = SequenceMatcher(None, Zutat, str(Neue_Zutat)).ratio()
		if Aehnlichkeit == 1:
			return 0
		elif Aehnlichkeit > 0.9:
			Abfrage = input("Große Ähnlichkeit zwischen %s und %s gefunden. Trotzdem eintragen? [y,N]" % (Zutat, Neue_Zutat))
			if not Abfrage == 'y':
				print("Zutat %s wird nicht eingetragen und im Rezept durch %s ersetzt" % (Neue_Zutat, Zutat))
				return Zutat # gibt Vorhandene Zutat mit großer Ähnlichkeit zu neuer Zutat zurück
		
	try:
		c.execute('''INSERT INTO Zutaten (Zutaten_Name) VALUES ("%s")''' % Neue_Zutat)
		conn.commit()
	except sqlite3.IntegrityError:
		print("Die Zutat %s ist schon vorhanden." % Neue_Zutat)
	print("Zutat %s eingetragen" % Neue_Zutat)
	return 0
	
# Trägt Neue Mengeneinheit ein.
# Gibt 0 zurück, falls erfolgreich
# Gibt eine ME zurück, falls ME mit großer Ähnlichkeit gefunden wurde.
def Neue_Mengeneinheit_Eintragen(Neue_Mengeneinheit):
	Alle_Mengeneinheiten = c.execute('''SELECT Einheit_name FROM Einheit''')
	
	for Einheit in Alle_Mengeneinheiten:
		Einheit = str(Einheit)[2:-3]
		Aehnlichkeit = SequenceMatcher(None, Einheit, str(Neue_Mengeneinheit)).ratio()
		if Aehnlichkeit == 1:
			return 0
		elif Aehnlichkeit > 0.9:
			Abfrage = input("Große Ähnlichkeit zwischen der neuen Mengeneinheit %s und der Vorhandenen %s gefunden. Trotzdem eintragen? [y,N]" % (Neue_Mengeneinheit, Einheit))
			if not Abfrage == 'y':
				print("Mengenheinheit %s wird nicht eingetragen und im Rezept durch %s ersetzt" % (Neue_Mengeneinheit, Einheit))
				return Einheit # gibt Vorhandene Einheit mit großer Ähnlichkeit zu neuer Zutat zurück
	try:
		c.execute('''INSERT INTO Einheit (Einheit_name) VALUES ("%s")''' % Neue_Mengeneinheit)
		conn.commit()
	except sqlite3.IntegrityError:
		pass
		#print("Die Mengeneinheit %s ist schon vorhanden." % Neue_Mengeneinheit)
	print("Mengeneinheit %s hinzugefügt" % Neue_Mengeneinheit)
	return 0

# gibt 0 zurück, wenn Rezeptname neu und damit Eindeutig ist
# gibt einen neuen Namen zurück, wenn er geändert werden musste
def Rezeptnamen_auf_Eindeutigkeit_pruefen(Neuer_Rezeptname):
	Eingangsname = Neuer_Rezeptname
	
	unklar = True
	Rezeptname_geaendert = False
	while unklar:
		Rezeptname_geaendert = False
		Alle_Rezeptnamen = c.execute('''SELECT Rezept_Name FROM Rezept_Name''')
		for Rezeptname in Alle_Rezeptnamen:
			Rezeptname = str(Rezeptname)[2:-3]
			if Neuer_Rezeptname == Rezeptname:
				Rezeptname_geaendert = True
				Neuer_Rezeptname = input("Das Rezept %s gibt es schon, gib dem Neuen bitte einen anderen Namen: " % Neuer_Rezeptname)
		if not Rezeptname_geaendert:
			unklar = False
	
	return Neuer_Rezeptname

def Rezept_Eintragen(rezept):
	# erwartet ein Dictionary folgender Struktur:
	# 'Name':Name des Rezepts
	# 'Personenzahl':Anzahl der Personen, für die das Rezept ausgelegt ist
	# 'Anweisungen': Zubereitungsanweisungen
	# 'Kategorie': Liste von Kategorien, in die das Rezept fällt
	# 'Zutaten': Liste von Zutaten: Zutaten[0] = Name der Zutat
	#                               Zutaten[1] = Menge
	#                               Zutaten[2] = Mengeneinheit
	
	c.execute('''INSERT INTO Rezept_Name (Rezept_Name, Anweisung, Personenzahl) VALUES ("%s","%s","%s")''' % (rezept['Name'], rezept['Anweisungen'], rezept['Personenzahl']))
	
	for Zutat in rezept['Zutaten']:
		c.execute('''INSERT INTO Rezeptverwaltung (Rezept_Name, Zutat, Menge, Einheit) VALUES ("%s","%s","%s","%s")''' % (rezept['Name'], Zutat[0], Zutat[1], Zutat[2]))
	conn.commit()

def Rezept_Anlegen():
	Neu_Rezeptname = input("Bitte Rezeptnamen eingeben: ")
	Neu_Rezeptname = Rezeptnamen_auf_Eindeutigkeit_pruefen(Neu_Rezeptname)
	Neu_Personenzahl = input("Fuer wieviele Personen? ")
	Neu_Zutaten = []
	Mehr_Zutaten = True
	Zutat_Index = 1
	while Mehr_Zutaten:
		Neu_Zutat = input("Zutat %i: " % Zutat_Index)
		Neu_Menge = input("Menge? (Ohne Einheit) ")
		Neu_Einheit = input("Mengeneinheit: ")
		Zutat_schon_da = Neue_Zutat_Eintragen(Neu_Zutat)
		if Zutat_schon_da:
			Neu_Zutat = Zutat_schon_da
		
		ME_schon_da = Neue_Mengeneinheit_Eintragen(Neu_Einheit)
		if ME_schon_da:
			Neu_Einheit = ME_schon_da
			
		Neu_Zutaten.append([Neu_Zutat, Neu_Menge, Neu_Einheit])
		Nochwas = input("Weitere Zutat? [Y,n]")
		if Nochwas == "n":
			Mehr_Zutaten = False
		Zutat_Index = Zutat_Index + 1
	
	Neu_Anweisungen = input("Bitte Anweisungen eingeben: ")
	Rezept = {'Name':Neu_Rezeptname,'Personenzahl':Neu_Personenzahl,'Zutaten':Neu_Zutaten,'Anweisungen':Neu_Anweisungen}
	print("==============================================================")
	print("%s für %s Personen" % (Neu_Rezeptname, Neu_Personenzahl))
	print("-------------------------------------------------------------")
	print("Zutaten:")
	Zutat_Rezept = []
	for Zutat in Neu_Zutaten:
		print("%s\t%s %s" % (Zutat[0], Zutat[1], Zutat[2]))
		Zutat_Rezept.append([Zutat[0], Zutat[1], Zutat[2]]) 
	print(" ")
	
	Rezept_Eintragen(Rezept)
	
	# Hier geschieht das Eigentliche Anlegen:
	# c.execute('''INSERT INTO Rezept_Name (Rezept_Name, Anweisung, Personenzahl) VALUES ("%s","%s","%s")''' % (Neu_Rezeptname, Neu_Anweisungen, Neu_Personenzahl))
	# for Zutat in Zutat_Rezept:
		# c.execute('''INSERT INTO Rezeptverwaltung (Rezept_Name, Zutat, Menge, Einheit) VALUES ("%s","%s","%s","%s")''' % (Neu_Rezeptname, Zutat[0], Zutat[1], Zutat[2]))
	# conn.commit()
				
conn = sqlite3.connect('COPY_OF_Rezeptverwaltung_SQLite.db3')
c = conn.cursor()
print("Rezeptdatenbank geöffnet")
Rezept_Anlegen()
conn.close()


