import re

class Utilities:
	def condition_decimal_point(self, string_with_number):
		res = re.findall('\d,\d',string_with_number)
		if res:
			res = res[0]
			ind = string_with_number.find(res)
			return string_with_number[:ind+1] + "." + string_with_number[ind+2:]
		else:
			return string_with_number

	def extract_number_from_string(self, suspect_string):
		if type(suspect_string) == str:
			number = re.findall(r'[\d\.]+', suspect_string.replace(',','.'))
			if number == []:
				return None
			else:
				return float(number[0])
		else:
			return suspect_string

	def get_data_from_form(self, request):
		if request.method == 'POST':
			data_from_form = request.form
		return data_from_form