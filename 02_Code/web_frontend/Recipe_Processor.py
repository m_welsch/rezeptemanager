import pudb
from web_frontend.recipe_data_structures import *
from web_frontend.Chefkoch_De_Parser import *


class Recipe_Processor():
	def __init__(self, debug = False):
		self.debug = debug
		self._CDP = Chefkoch_De_Parser()
	
	def recipe_valid(self, recipe, autocorrect = True):
		valid = True
		if recipe["Name"] == "":
			valid = False
		if recipe["Personenzahl"] == None:
			if autocorrect:
				recipe["Personenzahl"] = 1
			else:
				valid = False
		if recipe["Zutaten"] == []:
			valid = False
		return valid

	def create_recipe_from_source(self, data_from_form):
		recipe_source = data_from_form['source']
		if recipe_source == "manual":
			recipe = self.create_recipe_from_form(data_from_form)
		elif recipe_source == "chefkoch":
			recipe = self.create_recipe_from_chefkoch(data_from_form['ck_url'])
		return recipe

	def create_recipe_from_form(self, data_form):
		recipe = Recipe()
		recipe.name = data_form['Rezeptname']
		recipe.categories = self.cleanup_categories(data_form.getlist('Kategorien'))
		recipe.portions = self.cleanup_portions(data_form['Personenzahl'])
		recipe.instructions = self.cleanup_instructions(data_form['Anweisungen'])
		recipe.ingredients = self.cleanup_ingredients(data_form.getlist('Menge'), data_form.getlist('Mengeneinheit'), data_form.getlist('Zutat'))
		return recipe.output_recipe_dict()

	def create_recipe_from_chefkoch(self, url):
		recipe = Recipe()
		recipe_from_chefkoch = self._CDP.url_to_recipe(url)
		recipe.name = recipe_from_chefkoch["name"]
		recipe.categories = recipe_from_chefkoch["categories"]
		recipe.portions = recipe_from_chefkoch["portions"]
		recipe.instructions = recipe_from_chefkoch["instructions"]
		recipe.ingredients = recipe_from_chefkoch["ingredients"]
		return recipe.output_recipe_dict()

	def cleanup_portions(self, portions_raw):
		return float(self.find_number_in_string(portions_raw)[0])

	def cleanup_categories(self, categories_raw):
		# source: https://stackoverflow.com/questions/3845423/remove-empty-strings-from-a-list-of-strings#3845453
		return list(filter(None, categories_raw))

	def transpose_ingredients(self, amounts, units, ingredients_raw):
		'''returns a list of named tuples in the format
			[[amount1, unit1, ing1], [amount2, unit2, ing2]]'''
		def put_ingredient_in_nt(ing_list):
			if ing_list[0]:
				amount = float(ing_list[0])
			else:
				amount = None
			return Ingredient(amount, ing_list[1], ing_list[2])
		return list(map(put_ingredient_in_nt, zip(*[amounts, units, ingredients_raw])))

	def cleanup_ingredients(self, amounts, units, ingredients_raw):
		ingredients_transposed = self.transpose_ingredients(amounts, units, ingredients_raw)
		for ing_t in ingredients_transposed:
			pass
		return ingredients_transposed

	def cleanup_instructions(self, instructions_raw):
		return instructions_raw.strip()

	def find_number_in_string(self, suspect_string):
		return re.findall(r'[\d\.]+', suspect_string)	