import json
import pudb
import re
import urllib.request
from bs4 import BeautifulSoup
from web_frontend.recipe_data_structures import *
from web_frontend.utilities import *

class Chefkoch_De_Parser:

	def __init__(self):
		self.utils = Utilities()

	def url_to_recipe(self, url):
		html = self.download_html(url)
		schema = self.extract_schema_from_html(html)
		chefkoch_dict = schema#self.schema_to_dict(schema)
		recipe = self.chefkoch_dict_to_recipe(chefkoch_dict, url)
		return recipe

	def download_html(self, url):
		return urllib.request.urlopen(url).read()

	def extract_schema_from_html(self, html):
		soup = BeautifulSoup(html, 'lxml')
		scripts_iterator = iter(soup.find_all('script'))
		searching = True
		for script in scripts_iterator:
			try:
				if script.attrs['type'] == 'application/ld+json':
					d = json.loads("".join(script))
					if d['@type'] == 'Recipe':
						return d
			except:
				pass

	def schema_to_dict(self, schema):
		invalid_chars = ['\n']
		for invalid_char in invalid_chars:
			schema = schema.replace(invalid_char,'')
		d = json.loads(schema)
		return d

	def chefkoch_dict_to_recipe(self, chefkoch_dict, url):
		recipe = {}
		recipe["name"] = chefkoch_dict["name"]
		recipe["categories"] = self.extract_categories_from_chefkoch_dict(chefkoch_dict["recipeCategory"])
		recipe["portions"] = self.extract_portions_from_chefkoch_dict(chefkoch_dict["recipeYield"])
		recipe["ingredients"] = self.extract_ingredients_from_chefkoch_dict(chefkoch_dict["recipeIngredient"])
		recipe["instructions"] = self.extract_instructions_from_chefkoch_dict(chefkoch_dict["recipeInstructions"], url)
		#recipe["Beschreibung"] = chefkoch_dict["description"]
		return recipe

	def extract_categories_from_chefkoch_dict(self, ck_categories):
		if type(ck_categories) == str:
			return [ck_categories]
		else:
			return ck_categories

	def extract_portions_from_chefkoch_dict(self, recipeYield):
		persons = re.findall('\d+', recipeYield)
		if persons == []:
			persons = 0
		return float(persons[0])

	def extract_ingredients_from_chefkoch_dict(self, chefkoch_ingredients):
		"""Cases to cover:

		1) Ingredient with Amount and Unit
		   2,00 Liter Gemüsebrühe 
		   |    |     |
		   |    |     Ingredient
		   |    Unit  |
		   Amount |   |
		or |      |   |
		   250,00 g Möhre(n), frisch, TK oder notfalls aus der Dose

		2) Ingredient with Amount
		   3,00 Kartoffeln
		   |    |
		   |    Ingredient
		   Amount

		3) Ingredient with Unit only (most difficult case)
		   nach Geschmack Salz
		   |              |
		   |              Ingredient
		   Unit

		   Special case of 3): Example: 'Salz nach Belieben'

		4) Ingredient only (without Amount nor Unit)
		   Balsamico
		   |
		   Ingredient


		"""

		ingredients_parsed = []
		for chefkoch_ingredient in chefkoch_ingredients:
			chefkoch_ingredient = self.utils.condition_decimal_point(chefkoch_ingredient).strip()

			if self.utils.extract_number_from_string(chefkoch_ingredient) == None:
				"""no amount given: case 3 or 4 (see above)"""
				amount = None

				if not chefkoch_ingredient.find(" ") == -1: 
					"""case 3: contains whitespace, so probably unit and ingredient name"""
					unit, ingredient = self.process_case_3(chefkoch_ingredient)

				else:
					"""no whitespace, probably case 4"""
					unit, ingredient = self.process_case_2_or_4(chefkoch_ingredient)

			else:
				"""case 1 or 2: it contains a number (an amount)"""

				amount = self.utils.extract_number_from_string(chefkoch_ingredient) # converting , to . since chefkoch uses , as decimal point
				chefkoch_ingredient = self.remove_amount_from_ingredient(chefkoch_ingredient)

				if chefkoch_ingredient.find(" ") == -1:
					"""case 2: no whitespaces -> probably only ingredient left"""
					unit, ingredient = self.process_case_2_or_4(chefkoch_ingredient)

				else:
					"""case 1: contains whitespaces"""
					unit, ingredient = self.process_case_1(chefkoch_ingredient)

					
			ingredients_parsed.append(Ingredient(amount, unit, ingredient.strip()))
		return ingredients_parsed

	def process_case_3(self, chefkoch_ingredient):
		if chefkoch_ingredient.endswith("nach Belieben"):
			"""special case. Example: 'Salz nach Belieben'"""
			unit = "nach Belieben"
			ingredient = chefkoch_ingredient[:chefkoch_ingredient.index("nach Belieben")]
		else:
			chefkoch_ingredient = chefkoch_ingredient.split(" ")
			unit = chefkoch_ingredient[0]
			ingredient = " ".join(chefkoch_ingredient[1:])
		return unit, ingredient

	def process_case_2_or_4(self, chefkoch_ingredient):
		unit = ""
		ingredient = chefkoch_ingredient.strip()
		return unit, ingredient

	def process_case_1(self, chefkoch_ingredient):
		chefkoch_ingredient = chefkoch_ingredient.split(" ")
		unit = chefkoch_ingredient[0]
		ingredient = " ".join(chefkoch_ingredient[1:])
		return unit, ingredient			

	def remove_amount_from_ingredient(self, ingredient):
		amount = self.utils.extract_number_from_string(ingredient)
		return ingredient[(len(str(amount))-1):].strip()

	def extract_instructions_from_chefkoch_dict(self, chefkoch_instructions, source_url):
		return chefkoch_instructions + ' Quelle: {}'.format(source_url)

if __name__ == '__main__':
	CP = Chefkoch_Parser()

	mockup_url = 'https://www.chefkoch.de/rezepte/3022511455043635/Kuerbissuppe-mit-einem-Hauch-Curry.html'
	print(CP.url_to_recipe(mockup_url))
