import re
import collections as col

Ingredient = col.namedtuple('ingredient',['amount', 'unit', 'ingredient'])

class Recipe():
	def __init__(self, name = None, categories = [], portions = None, ingredients = None, instructions = ""):
		self.name = name
		self.categories = categories
		self.portions = portions
		self.ingredients = ingredients
		self.instructions = instructions

	def output_recipe_dict(self):
		return {"name":self.name,
				"portions":self.portions,
				"categories":self.categories,
				"ingredients":self.ingredients,
				"instructions":self.instructions}
	

class shopping_list():
	def __init__(self, name, recipes, notes):
		self._name = name
		self._recipes = self.validate_recipes(recipes)
		self._notes = notes

	def validate_recipes(self, recipes):
		for recipe in recipes:
			if not recipe:
				pass

if __name__ == '__main__':
	R = Recipe("Blaukraut")
	print(R.name)