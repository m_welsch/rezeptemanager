# -*- coding: utf-8 -*-

from flask import Flask, redirect, url_for, render_template, request
from web_frontend.Chefkoch_De_Parser import *
from web_frontend.Database_Communicator import *
from web_frontend.Recipe_Processor import *
import pudb
import git
import sys

DB = Database_Communicator()
CDP = Chefkoch_De_Parser(DB)
RP = Recipe_Processor(DB)
old_recipe = {'Name':'',
			  'Personenzahl':'',
			  'Kategorien':[],
			  'Zutaten':['', #Menge
			  			 '', #Mengeneinheit
			  			 '', #Zutat
			  			 '', #index
			  			 '', #Zutat_Anzeigen
			  			 '', #Zutat_Neu_Anzulegen
			  			 '', #ME_Anzeigen
			  			 ''], #ME_Neu_Anzulegen'
			  'Anweisungen':''}

application = Flask(__name__)

def load_top_menue():
	with open('templates/top_menue.html','r') as top_menue_file:
		top_menue = top_menue_file.read()

def get_commit():
	repo = git.Repo('../')
	commit_id = str(repo.git.log())[7:14]
	commit_date = repo.git.log().split('\n')[2][8:]
	commit_branch = repo.git.status().split("branch")[1].split('\n')[0][1:]
	return commit_id + ' from ' + commit_date + ' on branch '+ commit_branch

@application.route('/info')
def show_info():
	repo = git.Repo('../')
	commit_id = str(repo.git.log())[7:14]
	commit_date = repo.git.log().split('\n')[2][8:]
	commit_branch = repo.git.status().split("branch")[1].split('\n')[0][1:]
	info = {'Version': "rev1",
			'git Repo URL': 'https://bitbucket.org/m_welsch/rezeptemanager.git',
			'git Commit ID': commit_id,
			'git Commit Date': commit_date,
			'git Branch': commit_branch,
			'Datenbank': DB.database,
			'Gesamtzahl Rezepte': "tbd",
			'Gesamtzahl Zutaten': "tbd",
			'Python Version': sys.version,
			'pysqlite Version': DB.sqlite3_version,
			'sqlite3 Version': DB.sqlite_3_sqlite_version
			#'Flask Version': flask.__version__
			}

	return render_template('info.html', info = info)

@application.route('/')
def mainpage():
	all_recipe_names_with_categories = DB.get_all_recipe_names_with_categories()
	return render_template('search_recipe.html', 
							all_recipe_names_with_categories = all_recipe_names_with_categories,
							commit = get_commit())

@application.route('/<rezeptname>')
def show_recipe(rezeptname):
	rezept = DB.get_recipe(rezeptname)
	return render_template('show_recipe.html', rezept = rezept)

@application.route('/search_by_ingredient', methods = ['GET', 'POST'])
def search_recipe_by_ingredient():
	if request.method == 'POST':
		data_from_form = request.form
		ingredients_filter = data_from_form.getlist('ingredients_filter')

	all_recipes_with_ingredients = DB.get_all_recipes_with_ingredients()
	all_ingredients = DB.get_all_ingredients()
	return render_template('search_by_ingredient.html', 
							all_ingredients = all_ingredients,
							all_recipes_with_ingredients = all_recipes_with_ingredients)

@application.route('/<rezeptname>_edit', methods = ['POST'])
def edit_recipe(rezeptname):
	recipe = DB.get_recipe(rezeptname)
	all_recipe_names = DB.get_all_recipe_names()
	all_ingredients = DB.get_all_ingredients()
	all_units = DB.get_all_units()
	all_categories = DB.get_all_categories()
	old_recipe = recipe
	return render_template('new_recipe_review.html', 
							   recipe = recipe,
							   new_or_edit = "edit",
							   all_recipe_names = all_recipe_names,
							   all_ingredients = all_ingredients,
							   all_units = all_units,
							   all_categories = all_categories)

@application.route('/<rezeptname>_delete', methods = ['POST', 'GET'])
def delete_recipe(rezeptname):
	if request.method == 'POST':
		sure_to_delete = "no_decision"
		try:
			sure_to_delete = request.form['sure_you_want_to_delete']
		except:
			pass

		if sure_to_delete == "yes":
			print("deleting {}".format(rezeptname))
			DB.delete_recipe_by_name(rezeptname)
			return render_template('message.html', text_for_title = "Löschen erfolgreich", text_for_h2 = "{} wurde erfolgreich gelöscht!".format(rezeptname))

		elif sure_to_delete == "no":
			return render_template('message.html', text_for_title = "Löschen abgebrochen!", text_for_h2 = "Löschen abgebrochen!")
		else:
			return render_template('delete_recipe.html', rezeptname = rezeptname)

@application.route('/new_recipe')
def new_recipe():
	all_recipe_names = DB.get_all_recipe_names()
	all_ingredients = DB.get_all_ingredients()
	all_units = DB.get_all_units()
	all_categories = DB.get_all_categories()

	return render_template('new_recipe.html', recipe = old_recipe,
									   all_recipe_names = all_recipe_names,
									   all_ingredients = all_ingredients,
									   all_units = all_units,
									   all_categories = all_categories)

@application.route('/new_recipe_review', methods = ['POST', 'GET'])
def new_recipe_review():
	global old_recipe
	# pudb.set_trace()
	if request.method == 'POST':
		data_from_form = request.form
		recipe_source = data_from_form['source']
		recipe_review_loop = data_from_form['review_runde']
		recipe_changed = data_from_form['recipe_changed']
		recipe_new_or_edit = data_from_form['new_or_edit']

	if recipe_source == "manual":
		recipe = RP.create_recipe_dict_from_form(data_from_form)
	elif recipe_source == "chefkoch":
		cde_url = data_from_form['chefkoch_url']
		recipe = CDP.url_to_recipe(cde_url)
	else:
		return ("invalid recipe source: {}".format(recipe_source))

	new_ingredients = DB.get_new_ingredients(recipe)
	new_units = DB.get_new_units(recipe)

	# display recipe for review, if it comes right from the input dialog or if it has been changed in the last round
	if recipe_review_loop == "0" or recipe_changed == "yes":
		all_recipe_names = DB.get_all_recipe_names()
		all_ingredients = DB.get_all_ingredients()
		all_units = DB.get_all_units()
		all_categories = DB.get_all_categories()
		old_recipe = recipe
		return render_template('new_recipe_review.html', 
							   recipe = recipe,
							   new_or_edit = recipe_new_or_edit,
							   all_recipe_names = all_recipe_names,
							   all_ingredients = all_ingredients,
							   all_units = all_units,
							   all_categories = all_categories,
							   new_ingredients = new_ingredients,
							   new_units = new_units)

	elif recipe_review_loop == "1":
		if recipe_new_or_edit == "new":
			DB.add_ingredient_to_database(new_ingredients)
			DB.add_unit_to_database(new_units)
			DB.add_recipe_to_database(recipe)
			return render_template('message.html', text_for_title="Rezept eingetragen!", text_for_h2 = "{} wurde erfolgreich eingetragen!".format(recipe['Name']))

		elif recipe_new_or_edit == "edit":
			DB.edit_recipe(recipe)
			return render_template('message.html', text_for_title="Rezept geändert!", text_for_h2 = "{} wurde erfolgreich geändert!".format(recipe['Name']))
	else:
		return "Fehler."
@application.route('/search_by_ingredient')
def search_by_ingredient():
	return render_template('message.html', text_for_title="tbd", text_for_h2 = "tbd.")

@application.route('/import_recipe_from_chefkoch')
def import_recipe_from_chefkoch():
	return render_template('import_recipe_from_chefkoch.html')

@application.route('/shopping_lists')
def shopping_lists():
	all_shopping_lists = DB.all_shopping_lists
	return render_template('search_shopping_lists.html',
							all_shopping_lists = all_shopping_lists)

@application.route('/sl_show_<sl_name>')
def sl_show(sl_name):
	sl = DB.get_shopping_list(sl_name)
	bom = DB.get_bom_from_sl(sl_name)
	return render_template('show_shopping_list.html', sl = sl, bom = bom)

@application.route('/sl_edit_<sl_name>', methods = ['GET', 'POST'])
def sl_edit(sl_name):
	all_recipe_names = DB.get_all_recipe_names()
	all_shopping_list_names = DB.all_shopping_list_names
	sl = DB.get_shopping_list(sl_name)
	if sl_name == "new":
		sl_new_or_edit = "new"
	else:
		sl_new_or_edit = "edit"

	return render_template('sl_edit.html',
							sl = sl,
							all_recipe_names = all_recipe_names,
							all_shopping_list_names = all_shopping_list_names,
							new_or_edit = sl_new_or_edit)

@application.route('/sl_save', methods = ['GET', 'POST'])
def sl_save():
	if request.method == 'POST':
		data_from_form = request.form
		sl = RP.create_sl_dict_from_form(data_from_form)
		DB.save_shopping_list(sl)
		return redirect('sl_show_%s' % sl['Name'])

@application.route('/sl_delete_<sl_name>')
def sl_delete(sl_name):
	DB.delete_shopping_list(sl_name)
	return render_template('message.html', text_for_title="Einkaufsliste gelöscht",
						   				   text_for_h2="Einkaufsliste %s gelöscht" % sl_name)

@application.route('/add_recipe_to_sl_<recipe_name>', methods = ['GET', 'POST'])
def add_recipe_to_sl(recipe_name):
	quantity = 1
	if request.method == 'POST':
		quantity = request.form['quantity_for_sl']
		print(quantity)
	sl_name = DB.get_most_recent_shopping_list_name()
	if not sl_name == "":
		DB.add_recipe_to_shopping_list(sl_name, recipe_name, quantity)
	return redirect(url_for('mainpage'))

if __name__ == '__main__':
	load_top_menue()
	application.run(host = '0.0.0.0')