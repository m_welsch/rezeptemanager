class Shopping_List_Processor:

	def create_sl_dict_from_form(self, data_from_form):
		sl = {'Name':'',
			  'Notizen':'',
			  'Rezepte':['Rezeptname','Personenzahl']
			 }
		sl['Name'] = data_from_form['Einkaufsliste_Name']
		sl['Notizen'] = data_from_form['Notizen']
		recipes_raw = data_from_form.getlist('Rezepte')
		persons_raw = data_from_form.getlist('Personenzahl')
		sl['Rezepte'] = self.create_recipe_list_from_form(recipes_raw, persons_raw)

		return sl

	def create_recipe_list_from_form(self, recipes_raw, persons_raw):
		recipes = []
		for index in range(len(recipes_raw)):
			if not recipes_raw[index] == '':
				current_recipe = recipes_raw[index]
				current_persons = persons_raw[index]
				recipe_names = [i[0] for i in recipes]
				if recipes_raw[index] in recipe_names:
					index_of_recipe_to_update = recipe_names.index(recipes_raw[index])
					persons_present = float(persons_raw[index_of_recipe_to_update])
					persons_new = float(persons_raw[index])
					recipes[index_of_recipe_to_update][1] = persons_present + persons_new
				else:
					recipes.append([recipes_raw[index], persons_raw[index]])

		return recipes