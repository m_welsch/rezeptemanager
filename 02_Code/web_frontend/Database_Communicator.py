import sqlite3
from difflib import SequenceMatcher
import pudb
from datetime import datetime
from web_frontend.recipe_data_structures import *
from web_frontend.utilities import *

class Database_Communicator():
	#def __init__(self, database = '../COPY_OF_Rezeptverwaltung_SQLite.db3'):
	def __init__(self, database = '/media/HDD/share/MundM/Rezepte/Rezeptverwaltung.db3'):
		self._all_shopping_lists = []
		self._database = database
		self.utils = Utilities()
		self.conn = sqlite3.connect(self._database, check_same_thread=False) #why is check_same_thread=False necessary?? Has worked without
		self.c = self.conn.cursor()

	@property
	def database(self):
		return self._database

	@property
	def sqlite3_version(self):
		return sqlite3.version

	@property
	def sqlite_3_sqlite_version(self):
		return sqlite3.sqlite_version

	def get_all_recipe_names(self):
		Alle_Rezepte = self.c.execute('''SELECT Rezept_Name from Rezept_Name''')
		Rezepte = []
		for Rezept in Alle_Rezepte:
			if not str(Rezept) == '(None,)':
				Rezepte.append(str(Rezept)[2:-3])
		return Rezepte		


	def get_all_recipe_names_with_categories(self):
		all_recipes_with_categories = []
		# list of lists ... see: [[name1 [cat1a, cat1b]] [name2 [cat2a, cat2b]]]

		all_recipe_names = self.get_all_recipe_names()
		for recipe_name in all_recipe_names:
			categories = self.get_category_by_recipe_name(recipe_name)

			all_recipes_with_categories.append([recipe_name, categories])

		return all_recipes_with_categories

	def get_all_recipes_with_ingredients(self):
		'''format: ['recipe_name1',['ingredient1','ingredient2' ... 'ingredient n'],
				    'recipe_name2',['ingredient1','ingredient2' ... 'ingredient n'],
				    ...]
		'''
		all_recipes_with_ingredients = []
		all_recipe_names = self.get_all_recipe_names()
		for recipe_name in all_recipe_names:
			ingredient_list = []
			for ingredient in self.c.execute('''SELECT Zutat from Rezeptverwaltung where Rezept_Name ="%s"''' % recipe_name):
				ingredient_list.append(str(ingredient)[2:-3])
			all_recipes_with_ingredients.append([recipe_name, ingredient_list])

		return all_recipes_with_ingredients



	def add_ingredient_to_database(self,Neue_Zutat):
		try:
			if type(Neue_Zutat) == str:
				self.c.execute('''INSERT INTO Zutaten (Zutaten_Name) VALUES ("%s")''' % Neue_Zutat)
			elif type(Neue_Zutat) == list:
				for neue_zutat in Neue_Zutat:
					self.c.execute('''INSERT INTO Zutaten (Zutaten_Name) VALUES ("%s")''' % neue_zutat)

			self.conn.commit()
		except sqlite3.IntegrityError:
			print("Die Zutat %s ist schon vorhanden." % Neue_Zutat)
			return 1
		print("Zutat %s eingetragen" % Neue_Zutat)
		return 0

	def add_unit_to_database(self, new_units):
		try:
			if type(new_units) == str:
				self.c.execute('''INSERT INTO Einheit (Einheit_name) VALUES ("%s")''' % new_units)
			elif type(new_units) == list:
				for new_unit in new_units:
					self.c.execute('''INSERT INTO Einheit (Einheit_name) VALUES ("%s")''' % new_unit)

			self.conn.commit()
		except sqlite3.IntegrityError:
			print("Die Mengeneinheit %s ist schon vorhanden." % new_units)
			return 1
		print("Einheit(en) %s eingetragen" % new_units)
		return 0

	def get_category_by_recipe_name(self, recipe_name):
		#pudb.set_trace()
		self.c.execute('''SELECT Kategorie from KatNamHilf where Rezept_Name="%s"''' % recipe_name)
		categories = []
		for row in self.c:
			categories.append(str(row)[2:-3])
		return categories

	# gibt ein Rezept-Dictionary zurück aus den Datensätzen von "rezeptname"
	def get_recipe(self, recipe_name):
		instructions, portions = self.c.execute('''SELECT Anweisung, Personenzahl FROM Rezept_Name WHERE Rezept_Name ="%s"''' % recipe_name).fetchone()
		ingredients = []
		for row in self.c.execute('''SELECT Menge, Einheit, Zutat from Rezeptverwaltung where Rezept_Name ="%s"''' % recipe_name):
			amount = self.utils.extract_number_from_string(row[0])
			ingredients.append(Ingredient(amount, row[1], row[2]))

		categories = self.get_category_by_recipe_name(recipe_name)

		recipe_dict = {'name':recipe_name,'portions':portions,'categories':categories,'ingredients':ingredients,'instructions':instructions}
		return recipe_dict

	def add_recipe_to_database(self, rezept):
		# erwartet ein Dictionary folgender Struktur:
		# 'Name':Name des Rezepts
		# 'Personenzahl':Anzahl der Personen, für die das Rezept ausgelegt ist
		# 'Anweisungen': Zubereitungsanweisungen
		# 'Kategorie': Liste von Kategorien, in die das Rezept fällt
		# 'Zutaten': Liste von Zutaten: Zutaten[0] = Menge
		#                               Zutaten[1] = Mengeneinheit
		#                               Zutaten[2] = Name der Zutat
		
		self.c.execute('''INSERT INTO Rezept_Name (Rezept_Name, Anweisung, Personenzahl) VALUES ("%s","%s","%s")''' % (rezept['Name'], rezept['Anweisungen'].strip(), rezept['Personenzahl']))

		for category in rezept['Kategorien']:
			self.c.execute('''INSERT INTO KatNamHilf (Rezept_Name, Kategorie) VALUES ("%s","%s")''' % (rezept['Name'], category))
		
		for Zutat in rezept['Zutaten']:
			self.c.execute('''INSERT INTO Rezeptverwaltung (Rezept_Name, Zutat, Menge, Einheit) VALUES ("%s","%s","%s","%s")''' % (rezept['Name'], Zutat[2], Zutat[0], Zutat[1]))
		self.conn.commit()

	def delete_recipe_by_name(self, rezeptname):
		self.c.execute(''' DELETE FROM Rezept_Name WHERE Rezept_Name='%s'; ''' % rezeptname)
		self.c.execute(''' DELETE FROM Rezeptverwaltung WHERE Rezept_Name='%s';''' % rezeptname)
		self.c.execute(''' DELETE FROM KatNamHilf WHERE Rezept_Name='%s';''' % rezeptname)
		self.conn.commit()

	def delete_recipe(self, rezept):
		self.delete_recipe_by_name(rezept['Name'])

	def edit_recipe(self, rezept):
		# delete recipe first, then create a new entry
		self.delete_recipe(rezept)

		self.add_recipe_to_database(rezept)

	# Gibt eine Liste ähnlicher Zutaten zu "zutat" aus
	def find_similar_ingredients(self, zutat):
		Alle_Zutaten = self.c.execute('''SELECT Zutaten_Name FROM Zutaten''')
		Aehnliche_Zutaten = []
		for Vorhandene_Zutat in Alle_Zutaten:
			Vorhandene_Zutat = str(Vorhandene_Zutat)[2:-3] #Anfuehrungszeichen entfernen
			Aehnlichkeit = SequenceMatcher(None, Vorhandene_Zutat, str(zutat)).ratio()
			if Aehnlichkeit > 0.9:
				Aehnliche_Zutaten.append(Vorhandene_Zutat)
		
		return Aehnliche_Zutaten

	# returns the ingredients from the recipe that are not already in the database
	def get_new_ingredients(self, recipe):
		all_ingredients = self.get_all_ingredients()
		new_ingredients = []
		for ingredient in recipe["Zutaten"]:
			if not ingredient[2] in all_ingredients:
				new_ingredients.append(ingredient[2])
		return new_ingredients
		
	def get_new_units(self, recipe):
		all_units = self.get_all_units()
		new_units = []
		for ingredient in recipe["Zutaten"]:
			if not ingredient[1] in all_units:
				new_units.append(ingredient[1])
		return new_units

	def get_all_ingredients(self):
		Alle_Zutaten = []
		for Zutat in self.c.execute('''SELECT Zutaten_Name from Zutaten'''):
			Alle_Zutaten.append(str(Zutat)[2:-3])
		return Alle_Zutaten
		
	def get_all_units(self):
		Alle_Mengeneinheiten = []
		for ME in self.c.execute('''SELECT Einheit_Name from Einheit'''):
			Alle_Mengeneinheiten.append(str(ME)[2:-3])
		return Alle_Mengeneinheiten

	def get_all_categories(self):
		all_categories = []
		for ME in self.c.execute('''SELECT Kategorie from Kategorie'''):
			all_categories.append(str(ME)[2:-3])
		return all_categories

	# returns a list of similar units to "unit"
	def get_similar_units(self,unit):
		all_units = get_all_units()
		similar_units = []
		for existing_unit in all_units:
			similarity = SequenceMatcher(None, existing_unit, str(unit)).ratio()
			if similarity > 0.8:
				similar_units.append(existing_unit)

		return similar_units
		
	def get_recipes_by_category(self):
		Kategorien = []
		for kat in self.c.execute('''SELECT Kategorie from Kategorie'''):
			if not str(kat) == '(None,)':
				Kategorien.append(str(kat)[2:-3])
		list_categories = []
		for row in self.c.execute('''SELECT Kategorie, Rezept_Name from KatNamHilf'''):
			list_categories.append([str(row[0]), str(row[1])])
			
		return {'Kategorien':Kategorien,'list_categories':list_categories}
		
	def get_recipes_by_ingredient(self,ingredients):
		Suchkriterium = "WHERE "
		for Zutat_Suche in ingredients:
			Suchkriterium = Suchkriterium + 'Zutat = "' + Zutat_Suche + '" AND '
		#print("Suchstring: SELECT DISTINCT Rezept_Name from Rezeptverwaltung %s" % Suchkriterium[:-4])
		Ergebnisse = []
		
		self.c.execute('''SELECT Rezept_Name from Rezeptverwaltung %s''' % Suchkriterium[:-4])
		for row in self.c:
			print("%s" % row)
			Ergebnisse.append(str(row)[2:-3])
		#print("Ergebnisse: %r" % Ergebnisse)
		
		return Ergebnisse

	def create_shopping_list(self, sl_name):
		self.c.execute('''INSERT INTO Einkaufslisten_Verwaltung (Einkaufsliste_Name, Datum_Erstellung) VALUES ("%s", CURRENT_TIMESTAMP)''' % sl_name)
		self.conn.commit()

	def add_recipe_to_shopping_list(self, sl_name, recipe_name, quantity):
		#pudb.set_trace()
		self.c.execute('''SELECT Personenzahl FROM Einkaufslisten_Inhalt WHERE Rezept = "%s" AND Einkaufsliste_Name = "%s"''' % (recipe_name, sl_name))
		current_quantity = str(self.c.fetchone())[2:-3]
		if current_quantity == "":
		 	self.c.execute('''INSERT INTO Einkaufslisten_Inhalt (Einkaufsliste_Name, Rezept, Personenzahl) VALUES ("%s","%s","%s")''' % (sl_name, recipe_name, quantity))
		else:
			new_quantity = float(current_quantity) + float(quantity)
			self.c.execute('''UPDATE Einkaufslisten_Inhalt SET Personenzahl = "%s" WHERE Rezept = "%s" AND Einkaufsliste_Name = "%s"''' % (new_quantity, recipe_name, sl_name))

		self.conn.commit()

	def save_shopping_list(self, sl):
		# TODO: check if sl just has to be updated. Make it work properly
		sl_name = sl['Name']
		sl_notes = sl['Notizen']

		if self.c.execute('''SELECT Einkaufsliste_Name FROM Einkaufslisten_Verwaltung WHERE Einkaufsliste_Name = "%s"''' % sl_name).fetchone() == None:
			self.create_shopping_list(sl_name)

		self.c.execute('''UPDATE Einkaufslisten_Verwaltung 
						  SET Notizen = "%s", Datum_Aenderung = CURRENT_TIMESTAMP 
						  WHERE Einkaufsliste_Name = "%s"''' % (sl_notes, sl_name))

		for recipe in sl['Rezepte']:
			# pudb.set_trace()
			recipe_name = recipe[0]
			recipe_persons = recipe[1]

			if self.recipe_in_shopping_list(recipe_name, sl_name) == None:
				self.c.execute('''INSERT INTO Einkaufslisten_Inhalt (Einkaufsliste_Name, Rezept, Personenzahl) 
							  	  VALUES ("%s", "%s", "%s")''' % (sl_name, recipe_name, recipe_persons))
			else:
				self.c.execute('''UPDATE Einkaufslisten_Inhalt
								  SET Personenzahl = "%s"
								  WHERE Rezept = "%s"''' % (recipe_persons, recipe_name))
		self.conn.commit()

	def recipe_in_shopping_list(self, recipe_name, sl_name):
		return self.c.execute('''SELECT Personenzahl
								 FROM Einkaufslisten_Inhalt
								 WHERE Rezept = "%s" AND Einkaufsliste_Name = "%s"''' % (recipe_name, sl_name)).fetchone()

	def delete_recipe_from_shopping_list(self, sl_name, recipe_name):
		self.c.execute('''DELETE FROM Einkaufslisten_Inhalt WHERE Einkaufsliste_Name="%s" AND Rezept="%s"''' % (sl_name, recipe_name))
		self.conn.commit()

	def delete_shopping_list(self, sl_name):
		self.c.execute(''' DELETE FROM Einkaufslisten_Verwaltung WHERE Einkaufsliste_Name='%s';''' % sl_name)
		self.c.execute(''' DELETE FROM Einkaufslisten_Inhalt WHERE Einkaufsliste_Name='%s';''' % sl_name)
		self.conn.commit()

	@property
	def all_shopping_lists(self):
		self._all_shopping_lists = self.c.execute('''SELECT Einkaufsliste_Name, Datum_Erstellung FROM Einkaufslisten_Verwaltung''')
		return self._all_shopping_lists
	
	@property
	def all_shopping_list_names(self):
		all_sl_names = self._all_shopping_lists
		self._all_shopping_list_names = []
		for sl_name in all_sl_names:
			self._all_shopping_list_names.append(sl_name[0])
		return self._all_shopping_list_names

	def get_shopping_list(self, sl_name):
		sl = {'Name':'',
			  'Notizen':'',
			  'Datum_Erstellung':'',
			  'Rezepte':[]
			 }
		sl['Name'] = sl_name
		if not sl_name == "new":
			sl_data = (self.c.execute('''SELECT Notizen, Datum_Erstellung FROM Einkaufslisten_Verwaltung WHERE Einkaufsliste_Name="%s"''' % sl_name)).fetchone()
			sl['Notizen'] = str(sl_data[0])[2:-3]
			sl['Datum_Erstellung'] = sl_data[1]
			for recipe in self.c.execute(''' SELECT Rezept, SUM(Personenzahl) FROM Einkaufslisten_Inhalt WHERE Einkaufsliste_Name="%s" GROUP BY Rezept''' % sl_name):
				sl['Rezepte'].append(recipe)

		return sl

	def get_most_recent_shopping_list_name(self):
		most_recent_shopping_list_name = self.c.execute('''SELECT Einkaufsliste_Name from Einkaufslisten_Verwaltung order by Datum_Erstellung DESC LIMIT 1''')
		return str(most_recent_shopping_list_name.fetchone())[2:-3]

	def get_bom_from_sl(self, sl_name):
		bom = []
		for position in self.c.execute('''SELECT Menge, Einheit, Zutat, Rezept, Personenzahl 
										  FROM Einkaufslisten_Inhalt 
										  left join Rezeptverwaltung on (Einkaufslisten_Inhalt.Rezept = Rezeptverwaltung.Rezept_Name)
										  where Einkaufsliste_Name = "%s"''' % sl_name).fetchall():
			amount_in_recipe, unit, ingredient, recipe, persons_in_sl = position
			if ingredient == None or ingredient == '':
				continue
			persons_in_sl = float(persons_in_sl)
			persons_in_recipe = self.c.execute('''SELECT Personenzahl
														FROM Rezept_Name
														WHERE Rezept_Name = "%s"''' % recipe).fetchone()[0]
			persons_in_recipe = self.check_for_validity(persons_in_recipe, 1)
			ingredient = self.check_for_validity(ingredient, '')
			unit = self.check_for_validity(unit, '')
			if amount_in_recipe == None or amount_in_recipe == '':
				amount_in_sl = ''
			else:
				amount_in_sl = persons_in_sl / persons_in_recipe * amount_in_recipe
			
			bom.append([amount_in_sl, unit, ingredient])


		bom.sort(key=lambda tup: tup[2])  # sorts bom by ingredient

		# cumulate duplicate ingredients (like more than one recipe may include salt ...)
		index = 0
		while index < len(bom) - 1:
			cur_ing = bom[index][2]
			if bom[index][2] == bom[index+1][2] and bom[index][1] == bom[index+1][1]: # if same ingredient with same unit
				cur_quantity = float(str(bom[index][0]).replace(",","."))
				next_quantity = float(str(bom[index+1][0]).replace(",","."))
				bom[index][0] = str(cur_quantity + next_quantity)
				bom.pop(index+1)
			index += 1
		return bom

	def check_for_validity(self, testee, default):
		if testee == '' or testee == None:
			testee = default
		return testee