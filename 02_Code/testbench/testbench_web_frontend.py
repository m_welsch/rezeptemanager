from flask import Flask, redirect, url_for, render_template, request
from web_frontend.recipe_data_structures import *
from web_frontend.Recipe_Processor import *
from web_frontend.Database_Communicator import *
from web_frontend.utilities import *
import collections as col

application = Flask(__name__)
RP = Recipe_Processor()
DB = Database_Communicator()
utils = Utilities()

@application.route('/')
def show_overview_tests():
	return render_template('/overview_tests.html')

@application.route('/show_<recipe_name>')
def show_recipe(recipe_name):
	recipe = DB.get_recipe(recipe_name)
	return render_template('show_recipe.html', recipe = recipe)

@application.route('/show_json_<recipe_name>')
def show_json_recipe(recipe_name):
	recipe = DB.get_recipe(recipe_name)
	return recipe

@application.route('/new_recipe')
def new_recipe():
	return render_template('dummy_recipe.html')

@application.route('/new_recipe_review', methods = ['POST', 'GET'])
def new_recipe_review():
	data_from_form = utils.get_data_from_form(request)
	recipe = RP.create_recipe_from_source(data_from_form)
		
	return render_template('show_recipe.html', recipe = recipe)

@application.route('/named_tuple')
def can_flask_handle_named_tuples():
	nt = col.namedtuple('ingredient',['amount','unit','ingredient'])
	i1 = nt(1, 'ml', 'Wasser')
	i2 = nt(2, 'kg', 'Salat')
	nts = [i1, i2]
	return render_template('can_flask_handle_named_tuples.html', nts = nts)

@application.route('/test_ck')
def test_chefkoch_de_parser():
	url = 'https://www.chefkoch.de/rezepte/3022511455043635/Kuerbissuppe-mit-einem-Hauch-Curry.html'
	recipe = RP.create_recipe_from_chefkoch(url)
	return render_template('show_recipe.html', recipe = recipe)