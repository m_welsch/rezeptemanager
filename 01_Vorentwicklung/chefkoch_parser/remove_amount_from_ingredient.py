import re

test_ingredient = "300.00 g Linsen " # already conditioned (, replaced by .)

def remove_amount_from_ingredient(ingredient):
	amount = re.findall(r'[\d\.]+', ingredient)[0]
	return ingredient[len(amount):].strip()

print(remove_amount_from_ingredient(test_ingredient))


