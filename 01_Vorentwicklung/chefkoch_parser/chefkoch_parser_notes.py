import json
import pudb
import re
import urllib.request
from bs4 import BeautifulSoup

mockup_str = '''{
  "@context": "http://schema.org",
  "@type": "Recipe",
  "cookTime": "PT40M",
  "prepTime": "PT20M",
  "datePublished": "2004-03-02",
  "description": "Linseneintopf, ein gutes Rezept mit Bild aus der Kategorie Hülsenfrüchte. 76 Bewertungen: &amp;Oslash; 4,5. Tags: Eintopf, gekocht, Gemüse, Hauptspeise, Hülsenfrüchte, Schwein",
	"image": "https://img.chefkoch-cdn.de/ck.de/rezepte/18/18062/1144876-960x720-linseneintopf.jpg",
	"recipeIngredient": [
				  "300,00 g Linsen ",                        "2,00 EL Schmalz ",                        "1,00  Zwiebel(n) ",                        "2,00 Zehe/n Knoblauch , in feinen Würfeln",                        "1,00 Bund Lauchzwiebel(n) ",                        "4,00 große Kartoffel(n) ",                        "250,00 g Möhre(n) , frisch, TK oder notfalls aus der Dose",                        "3,00  Cabanossi ",                        "  Senf , süß",                        "  Balsamico ",                        "2,00 Liter Gemüsebrühe ",                        "  Liebstöckel ",                        "  Schnittlauch ",                        "  Worcestersauce ",                        "  Pfeffer ",                        "  Petersilie "            ],
  "name": "Linseneintopf",
  "author": {
	"@type": "Person",
	"name": "casi"
  },
	"recipeInstructions": "Die klein Zwiebel und die in Rllchen geschnittenen \nLauchzwiebeln im zerlassenen Schmalz and\u00fcnsten. Wenn die Zwiebeln leicht braun werden, erst dann die Knoblauchzehen hineindr\u00fccken und ebenfalls and\u00fcnsten. Dann die Gem\u00fcsebr\u00fche zugie\u00dfen. Die klein geschnittene Petersilie und den Liebst\u00f6ckel zuf\u00fcgen, aufkochen, die Linsen und die Kartoffeln (noch ganz und in Schale, nur sauber sollten sie sein) einwerfen. Alles ca. 35 - 40 Minuten k\u00f6cheln lassen. Die M\u00f6hren in Scheiben geschnitten nach gut einer Viertelstunde dazugeben. Die Kartoffeln kurz vor dem Weichkochen entnehmen, etwas abk\u00fchlen lassen, pellen und w\u00fcrfeln. Die Linsenbr\u00fche wieder w\u00e4rmen, eine Cabanossi in Scheiben schneiden, zwei ganz lassen. Die Cabanossi und die Kartoffelw\u00fcrfel wieder zuf\u00fcgen. Gut hei\u00df werden lassen und mit den \u00fcbrigen Gew\u00fcrzen und dem Senf abschmecken. Evtl. mit etwas Mehl andicken.",
  "recipeYield": "2"
  ,
  "aggregateRating": {
	"@type": "AggregateRating",
	"ratingValue": "4.45",
	"reviewCount": "76",
	"worstRating": 0,
	"bestRating": 5
  }
	,
	"recipeCategory": [
			  "Eintopf",              "gekocht",              "Gemüse",              "Hauptspeise",              "Hülsenfrüchte",              "Schwein"          ]
  }'''
class Chefkoch_Parser:

	def __init__(self, DB):
		self.DB = DB

	def url_to_recipe(self, url):
		html = self.download_html(url)
		schema = self.extract_schema_from_html(html)
		chefkoch_dict = schema#self.schema_to_dict(schema)
		recipe = self.chefkoch_dict_to_recipe(chefkoch_dict, url)
		return recipe

	def download_html(self, url):
		return urllib.request.urlopen(url).read()

	def extract_schema_from_html(self, html):
		soup = BeautifulSoup(html, 'lxml')
		scripts_iterator = iter(soup.find_all('script'))
		searching = True
		for script in scripts_iterator:
			try:
				if script.attrs['type'] == 'application/ld+json':
					d = json.loads("".join(script))
					if d['@type'] == 'Recipe':
						return d
			except:
				pass

	def schema_to_dict(self, schema):
		invalid_chars = ['\n']
		for invalid_char in invalid_chars:
			schema = schema.replace(invalid_char,'')
		d = json.loads(schema)
		return d

	def chefkoch_dict_to_recipe(self, chefkoch_dict, url):
		recipe = {}
		recipe["Name"] = chefkoch_dict["name"]
		recipe["Kategorien"] = chefkoch_dict["recipeCategory"]
		recipe["Personenzahl"] = self.extract_persons_from_chefkoch_dict(chefkoch_dict["recipeYield"])
		recipe["Zutaten"] = self.extract_ingredients_from_chefkoch_dict(chefkoch_dict["recipeIngredient"])
		recipe["Anweisungen"] = self.extract_instructions_from_chefkoch_dict(chefkoch_dict["recipeInstructions"], url)
		#recipe["Beschreibung"] = chefkoch_dict["description"]

		return recipe

	def extract_persons_from_chefkoch_dict(self, recipeYield):
		persons = re.findall('\d+', recipeYield)
		if persons == []:
			persons = ""
		return persons[0]

	def extract_ingredients_from_chefkoch_dict(self, chefkoch_ingredients):
		"""Cases to cover:

		1) Ingredient with Amount and Unit
		   2,00 Liter Gemüsebrühe 
		   |    |     |
		   |    |     Ingredient
		   |    Unit  |
		   Amount |   |
		or |      |   |
		   250,00 g Möhre(n), frisch, TK oder notfalls aus der Dose

		2) Ingredient with Amount
		   3,00 Kartoffeln
		   |    |
		   |    Ingredient
		   Amount

		3) Ingredient with Unit only (most difficult case)
		   nach Geschmack Salz
		   |              |
		   |              Ingredient
		   Unit

		4) Ingredient only (without Amount nor Unit)
		   Balsamico
		   |
		   Ingredient
		"""

		ingredients_parsed = []
		for chefkoch_ingredient in chefkoch_ingredients:
			chefkoch_ingredient = self.condition_decimal_point(chefkoch_ingredient)
			chefkoch_ingredient = chefkoch_ingredient.strip()

			# pudb.set_trace()
			if self.find_number_in_string(chefkoch_ingredient) == []:
				"""no amount given: case 3 or 4 (see above)"""

				if not chefkoch_ingredient.find(" ") == -1: 
					"""contains whitespace, so probably unit and ingredient name (case 3)"""

					chefkoch_ingredient = chefkoch_ingredient.split(" ")

					unit = chefkoch_ingredient[0]
					ingredient = ""
					for ingredient_part in chefkoch_ingredient[1:]:
						ingredient += " " + ingredient_part

				else:
					"""no whitespace, probably case 4"""
					unit = ""
					ingredient = chefkoch_ingredient

				amount = ""

			else:
				"""case 1 or 2 (see above), because it contains a number (an amount)"""

				amount = self.find_number_in_string(chefkoch_ingredient)[0] # converting , to . since chefkoch uses , as decimal point
				chefkoch_ingredient = self.remove_amount_from_ingredient(chefkoch_ingredient)

				if chefkoch_ingredient.find(" ") == -1:
					"""no whitespaces -> probably only ingredient left"""
					unit = ""
					ingredient = chefkoch_ingredient.strip()
				else:
					chefkoch_ingredient = chefkoch_ingredient.split(" ")
					unit = chefkoch_ingredient[0]
					ingredient = ""
					for ingredient_part in chefkoch_ingredient[1:]:
						ingredient += " " + ingredient_part
			ingredients_parsed.append([amount, unit, ingredient])
		return ingredients_parsed

	def condition_decimal_point(self, string_with_number):
		res = re.findall('\d,\d',string_with_number)
		if not res == []:
			res = res[0]
			ind = string_with_number.find(res)
			return string_with_number[:ind+1] + "." + string_with_number[ind+2:]
		else:
			return string_with_number

	def remove_amount_from_ingredient(self, ingredient):
		amount = re.findall(r'[\d\.]+', ingredient)[0]
		return ingredient[len(amount):].strip()

	def check_whether_first_word_is_known_unit(self, ingredient):
		suspect = ingredient.strip().split(" ")[0]
		all_units = self.DB.get_all_units()
		if suspect in all_units:
			return True
		else:
			return False

	def find_number_in_string(self, suspect_string):
		return re.findall(r'[\d\.]+', suspect_string)

	def extract_instructions_from_chefkoch_dict(self, chefkoch_instructions, source_url):
		return chefkoch_instructions + ' Quelle: {}'.format(source_url)

if __name__ == '__main__':
	from Database_Communicator import *
	DB = Database_Communicator()
	CP = Chefkoch_Parser(DB)

	mockup_url = 'https://www.chefkoch.de/rezepte/3022511455043635/Kuerbissuppe-mit-einem-Hauch-Curry.html'
	print(CP.url_to_recipe(mockup_url))
