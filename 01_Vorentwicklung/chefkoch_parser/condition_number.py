#!/usr/bin/python3

import re

myin = ["300,00 g Linsen ", "Geblubber"];

def condition_decimal_point(string_with_number):
	res = re.findall('\d,\d',string_with_number)
	if not res == []:
		res = res[0]
		ind = string_with_number.find(res)
		return string_with_number[:ind+1] + "." + string_with_number[ind+2:]
	else:
		return string_with_number

for it in myin:
	print(condition_decimal_point(it))