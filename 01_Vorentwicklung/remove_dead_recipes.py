import sqlite3
from Database_Communicator import *

DB = Database_Communicator()
conn = sqlite3.connect('../COPY_OF_Rezeptverwaltung_SQLite.db3', check_same_thread=False) #why is check_same_thread=False necessary?? Has worked without
c = conn.cursor()

all_recipes = DB.get_all_recipe_names()
recipes_with_ingredients = []
dead_recipes = []

for recipe_raw in c.execute('''SELECT Rezept_Name FROM Rezeptverwaltung'''):
	recipe_name = str(recipe_raw)[2:-3]
	if not recipe_name in recipes_with_ingredients:
		recipes_with_ingredients.append(recipe_name)

for recipe in all_recipes:
	if not recipe in recipes_with_ingredients:
		dead_recipes.append(recipe)

print(all_recipes)
print(recipes_with_ingredients)
print(dead_recipes)

for dead_recipe in dead_recipes:
	c.execute('''DELETE FROM Rezept_Name WHERE Rezept_Name = "%s" ''' % dead_recipe)

conn.commit()