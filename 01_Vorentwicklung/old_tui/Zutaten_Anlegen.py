import sqlite3
from difflib import SequenceMatcher

# Trägt Neue Zutat ein.
# Gibt 0 zurück, falls erfolgreich
# Gibt einen Zutatennamen zurück, falls Zutat mit großer Ähnlichkeit gefunden wurde.
def Neue_Zutat_Eintragen():
	Neue_Zutat = input("Bitte Neue Zutat eingeben: ")
	Alle_Zutaten = c.execute('''SELECT Zutaten_Name FROM Zutaten''')

	for Zutat in Alle_Zutaten:
		Zutat = str(Zutat)[2:-3] #Anfuehrungszeichen entfernen
		Aehnlichkeit = SequenceMatcher(None, Zutat, str(Neue_Zutat)).ratio()
		if Aehnlichkeit > 0.9:
			Abfrage = input("Große Ähnlichkeit zwischen %s und %s gefunden. Trotzdem eintragen? [y,N]" % (Zutat, Neue_Zutat))
			if not Abfrage == 'y':
				print("Zutat %s wird nicht eingetragen" % Neue_Zutat)
				return Zutat # gibt Vorhandene Zutat mit großer Ähnlichkeit zu neuer Zutat zurück
		
	try:
		c.execute('''INSERT INTO Zutaten (Zutaten_Name) VALUES ("%s")''' % Neue_Zutat)
		conn.commit()
	except sqlite3.IntegrityError:
		print("Die Zutat %s ist schon vorhanden." % Neue_Zutat)
	print("Zutat %s eingetragen" % Neue_Zutat)
	return 0
		
conn = sqlite3.connect('COPY_OF_Rezeptverwaltung_SQLite.db3')
c = conn.cursor()
print("Rezeptdatenbank geöffnet")
Neue_Zutat_Eintragen()
conn.close()


