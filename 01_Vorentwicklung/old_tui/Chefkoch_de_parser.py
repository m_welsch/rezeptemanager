# source: https://www.pythonforbeginners.com/beautifulsoup/beautifulsoup-4-python
#         http://www.compjour.org/warmups/govt-text-releases/intro-to-bs4-lxml-parsing-wh-press-briefings/

import urllib.request
from bs4 import BeautifulSoup

html = '''
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Recipe",
  "cookTime": "PT40M",
  "prepTime": "PT20M",
  "datePublished": "2004-03-02",
  "description": "Linseneintopf, ein gutes Rezept mit Bild aus der Kategorie Hülsenfrüchte. 76 Bewertungen: &amp;Oslash; 4,5. Tags: Eintopf, gekocht, Gemüse, Hauptspeise, Hülsenfrüchte, Schwein",
    "image": "https://img.chefkoch-cdn.de/ck.de/rezepte/18/18062/1144876-960x720-linseneintopf.jpg",
    "recipeIngredient": [
                  "300,00 g Linsen ",                        "2,00 EL Schmalz ",                        "1,00  Zwiebel(n) ",                        "2,00 Zehe/n Knoblauch , in feinen Würfeln",                        "1,00 Bund Lauchzwiebel(n) ",                        "4,00 große Kartoffel(n) ",                        "250,00 g Möhre(n) , frisch, TK oder notfalls aus der Dose",                        "3,00  Cabanossi ",                        "  Senf , süß",                        "  Balsamico ",                        "2,00 Liter Gemüsebrühe ",                        "  Liebstöckel ",                        "  Schnittlauch ",                        "  Worcestersauce ",                        "  Pfeffer ",                        "  Petersilie "            ],
  "name": "Linseneintopf",
  "author": {
    "@type": "Person",
    "name": "casi"
  },
    "recipeInstructions": "Die klein gehackte Zwiebel und die in R\u00f6llchen geschnittenen Lauchzwiebeln im zerlassenen Schmalz and\u00fcnsten. Wenn die Zwiebeln leicht braun werden, erst dann die Knoblauchzehen hineindr\u00fccken und ebenfalls and\u00fcnsten. Dann die Gem\u00fcsebr\u00fche zugie\u00dfen. Die klein geschnittene Petersilie und den Liebst\u00f6ckel zuf\u00fcgen, aufkochen, die Linsen und die Kartoffeln (noch ganz und in Schale, nur sauber sollten sie sein) einwerfen. \n\nAlles ca. 35 - 40 Minuten k\u00f6cheln lassen. Die M\u00f6hren in Scheiben geschnitten nach gut einer Viertelstunde dazugeben. Die Kartoffeln kurz vor dem Weichkochen entnehmen, etwas abk\u00fchlen lassen, pellen und w\u00fcrfeln. \n\nDie Linsenbr\u00fche wieder w\u00e4rmen, eine Cabanossi in Scheiben schneiden, zwei ganz lassen. Die Cabanossi und die Kartoffelw\u00fcrfel wieder zuf\u00fcgen. Gut hei\u00df werden lassen und mit den \u00fcbrigen Gew\u00fcrzen und dem Senf abschmecken. Evtl. mit etwas Mehl andicken.",
  "recipeYield": "2"
  ,
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.45",
    "reviewCount": "76",
    "worstRating": 0,
    "bestRating": 5
  }
    ,
    "recipeCategory": [
              "Eintopf",              "gekocht",              "Gemüse",              "Hauptspeise",              "Hülsenfrüchte",              "Schwein"          ]
  }
</script>
'''

class chefkoch_de_parser():
	def __init__(self):
		pass
		
	def parse_recipe(self,html):
		soup = BeautifulSoup(html, 'lxml')

		scripts_iterator = iter(soup.find_all('script'))
		searching = True
		description = ""
		name = ""
		ings = []
		instructions = ""
		persons = ""
		flag_ingredients_coming = False
		while searching:
			script = next(scripts_iterator)
			lines = str(script).split('\n')
			for line in lines:
				myline = line.lstrip()
				#print("current line: %s" % myline)
				# this is being called if the previous line held the "recipeIngredients"-Tag
				if flag_ingredients_coming:
					flag_ingredients_coming = False
					ing = myline.split('"')
					index = 1
					while index:
						if index > len(ing)-1:
							break
						ings_raw = ing[index].strip()
						ings_split = ings_raw.split(' ')
						while len(ings_split) > 3:
							#print("current splitting: %i" % len(ings_split))
							ings_split[len(ings_split) - 2] = ings_split[len(ings_split) - 2] + " "+ ings_split[len(ings_split) - 1]
							#print("ings_split: %r" % len(ings_split))
							del ings_split[-1]
						#print("len(ings_split) = %r" % len(ings_split))
						if len(ings_split) == 1: # falls nur Zutat und keine Mengenangabe
							ings.append([" ", "nach Wunsch", ings_split[0]])
						else:
							ings.append(ings_split)
						index = index + 2
					
							
				if myline[0:14] == '"description":':
					end_of_description = myline.find('Bewertungen')
					# ~ print("%s is decimal: %r" % (myline[end_of_description-2], myline[end_of_description-2].isdecimal()) )
					# ~ while myline[end_of_description-2].isdecimal():
						# ~ end_of_description = end_of_description -1
					description = myline[16:end_of_description-3].strip()
					#searching = False
				
				elif myline[0:6] == '"name"':
					if name == "":
						name = myline[7:].split('"')[1]
					
				elif myline[0:18] == '"recipeIngredient"':
					flag_ingredients_coming = True
					
				elif myline[0:20] == '"recipeInstructions"':
					instructions = myline[21:].split('"')[1]
					
				elif myline[0:13] == '"recipeYield"':
					persons = myline.split('"')[3]
								
				if description != "" and name != "" and ings != [] and instructions != "" and persons != "":
					print("Finished!")
					searching = False
					break

		recipe_dict = {"Name":name, "Beschreibung":description, "Zutaten":ings, "Anweisungen":instructions, "Personenzahl":persons}
		#print("Name: \n %s" % name)
		#print("Description: \n %s" % description)
		#print("Ingredients: \n %r" % ings)
		#print("Instructions: \n %s" % instructions)
		return recipe_dict

#chefkoch_url = input("Chefkoch-URL: ")
#html = urllib.request.urlopen(chefkoch_url).read()
cdp = chefkoch_de_parser()
print(cdp.parse_recipe(html))
