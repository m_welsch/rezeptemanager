import sqlite3
from difflib import SequenceMatcher
import readline

class MyCompleter(object):  # Custom completer

    def __init__(self, options):
        self.options = sorted(options)

    def complete(self, text, state):
        if state == 0:  # on first trigger, build possible matches
            if text:  # cache matches (entries that start with entered text)
                self.matches = [s for s in self.options 
                                    if s and s.startswith(text)]
            else:  # no text entered, all matches possible
                self.matches = self.options[:]

        # return match indexed by state
        try: 
            return self.matches[state]
        except IndexError:
            return None

conn = sqlite3.connect('COPY_OF_Rezeptverwaltung_SQLite.db3')
c = conn.cursor()
print("Rezeptdatenbank geöffnet")

Alle_Zutaten = c.execute('''SELECT Zutaten_Name from Zutaten''')

zutaten_eingabe = input("Bitte Zutaten eingeben (mit Komma getrennt): ")
zutaten_eingabe = zutaten_eingabe.split(',')
Zutaten_Suche = []

for Zutat_aus_Datenbank in Alle_Zutaten:
	Zutat_aus_Datenbank = str(Zutat_aus_Datenbank)[2:-3]
	for Zutat_aus_Eingabe in zutaten_eingabe:
		Zutat_aus_Eingabe.strip()
		if Zutat_aus_Eingabe == Zutat_aus_Datenbank:
			Zutaten_Suche.append(Zutat_aus_Eingabe)
		else:
			Aehnlichkeit = SequenceMatcher(None, str(Zutat_aus_Datenbank), Zutat_aus_Eingabe).ratio()
			Schwelle = 0.9
			if len(Zutat_aus_Eingabe) < 7:
				Schwelle = 0.1 * len(Zutat_aus_Eingabe) + 0.2
			if Aehnlichkeit > Schwelle:
				Entscheidung = input("Soll nach %s anstelle von %s gesucht werden? [Y,n]" % (Zutat_aus_Datenbank, Zutat_aus_Eingabe))
				if Entscheidung == "n":
					Zutaten_Suche.append(Zutat_aus_Eingabe)
				else:
					Zutaten_Suche.append(Zutat_aus_Datenbank)

Suchkriterium = "WHERE "
for Zutat_Suche in Zutaten_Suche:
	Suchkriterium = Suchkriterium + 'Zutat = "' + Zutat_Suche + '" AND '

Rezepte_Auswahl = []
c.execute('''SELECT DISTINCT Rezept_Name from Rezeptverwaltung %s''' % Suchkriterium[:-4])
for row in c:
	print("%s" % row)
	Rezepte_Auswahl.append(str(row)[2:-3])
print("")

completer = MyCompleter(Rezepte_Auswahl)
readline.set_completer(completer.complete)
readline.parse_and_bind('tab: complete')
rezept_eingabe = input("Rezeptnamen eingeben: ")

c.execute('''SELECT Anweisung, Personenzahl FROM Rezept_Name WHERE Rezept_Name ="%s"''' % rezept_eingabe)

for row in c:
	Anweisungen = row[0]
	Personenzahl = row[1]
print("================================")
print("%s für %s Personen" % (rezept_eingabe, Personenzahl))
print("================================")
print("Zutaten:")

c.execute('''SELECT Zutat, Menge, Einheit from Rezeptverwaltung where Rezept_Name ="%s"''' % rezept_eingabe)
for row in c:
	print("%s %s\t%s" % (row[1], row[2], row[0]))

print("\nAnweisungen:\n%s" % Anweisungen)
print("================================")

conn.close()
