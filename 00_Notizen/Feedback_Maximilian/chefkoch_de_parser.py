def _extract_schema_from_html(self, html):
	soup = BeautifulSoup(html, 'lxml')
	scripts_iterator = iter(soup.find_all('script'))
	for script in scripts_iterator:
		try:
			if script.attrs['type'] == 'application/ld+json':
				d = json.loads("".join(script))
				if d['@type'] == 'Recipe':
					return d
		except:  # Don't ignore all exceptions!
			passdefaultdict

def _extract_schema_from_html(self, html):
	soup = BeautifulSoup(html, 'lxml')
	scripts_iterator = iter(soup.find_all('script'))  # findall returns a list which can be iterated over already.
	for script in scripts_iterator:
		try:
			if script.attrs['type'] == 'application/ld+json':
				d = json.loads("".join(script))
				if d['@type'] == 'Recipe':
					return d
		except KeyError:
			pass

def _extract_schema_from_html(self, html):
	soup = BeautifulSoup(html, 'lxml')
	scripts = soup.find_all('script')
	for script in scripts:
		try:  # You can already filter out all scripts without "type" attribute via list comprehension.
			if script.attrs['type'] == 'application/ld+json':
				d = json.loads("".join(script))
				if d['@type'] == 'Recipe':
					return d
		except KeyError:
			pass

def _extract_schema_from_html(self, html):
	soup = BeautifulSoup(html, 'lxml')
	scripts = [s for s in soup.find_all('script') if "type" in s.attrs]
	for script in scripts:
		if script.attrs['type'] == 'application/ld+json':  # And you can filter out those with wrong types as well.
			d = json.loads("".join(script))
			if d['@type'] == 'Recipe':
				return d

def _extract_schema_from_html(self, html):
	soup = BeautifulSoup(html, 'lxml')
	scripts = [s for s in soup.find_all('script') if "type" in s.attrs and s.attrs["type"] == "application/ld+json"]  # Actually, BS can do that for you.
	for script in scripts:
		d = json.loads("".join(script))
		if d['@type'] == 'Recipe':
			return d

def _extract_schema_from_html(self, html):
	soup = BeautifulSoup(html, 'lxml')
	scripts = soup.find_all('script', {"type": "application/ld+json"})
	for script in scripts:
		d = json.loads("".join(script))
		if d['@type'] == 'Recipe':
			return d  # Little renaming because fancy...

def _extract_schema_from_html(self, html):  # No self -> make static
	soup = BeautifulSoup(html, 'lxml')
	scripts = soup.find_all('script', {"type": "application/ld+json"})
	for script in scripts:
		json_obj = json.loads("".join(script))
		if json_obj['@type'] == 'Recipe':
			return json_obj

@staticmethod
def _extract_schema_from_html(html):  # Rename since it's a recipe in json that is extracted.
	soup = BeautifulSoup(html, 'lxml')
	scripts = soup.find_all('script', {"type": "application/ld+json"})
	for script in scripts:
		json_obj = json.loads("".join(script))
		if json_obj['@type'] == 'Recipe':
			return json_obj

@staticmethod
def _extract_json_recipe_from_html(html):  # Since you want to work with recipe objects in the future, convert it immediately.
	soup = BeautifulSoup(html, 'lxml')
	scripts = soup.find_all('script', {"type": "application/ld+json"})
	for script in scripts:
		json_obj = json.loads("".join(script))
		if json_obj['@type'] == 'Recipe':
			return json_obj

from recipe_data_structures import Recipe

@staticmethod
def _extract_recipe_from_html(html):
	soup = BeautifulSoup(html, 'lxml')
	scripts = soup.find_all('script', {"type": "application/ld+json"})
	for script in scripts:
		json_obj = json.loads("".join(script))
		if json_obj['@type'] == 'Recipe':
			return Recipe.from_json(json_obj)

### recipe_data_structures.py:
class Recipe:
	# ...
	@classmethod
	def from_json(cls, recipe_json):
		return cls(name=recipe_json[""],
				   ingredients=recipe_json[""],
				   # ...
				   )
	# ...
###