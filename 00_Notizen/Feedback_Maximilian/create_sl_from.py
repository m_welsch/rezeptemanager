

Antwort!!!

def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = []
    for index in range(len(recipes_raw)):  # Loop directly over both (zipped) lists' elements
        if not recipes_raw[index] == '':
            current_recipe = recipes_raw[index]
            current_persons = persons_raw[index]
            recipe_names = [i[0] for i in recipes]
            if recipes_raw[index] in recipe_names:
                index_of_recipe_to_update = recipe_names.index(recipes_raw[index])
                persons_present = float(persons_raw[index_of_recipe_to_update])
                persons_new = float(persons_raw[index])
                recipes[index_of_recipe_to_update][1] = persons_present + persons_new
            else:
                recipes.append([recipes_raw[index], persons_raw[index]])




def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = []
    for raw_recipe, raw_persons in zip(recipes_raw, persons_raw):
        if not raw_recipe == '':
            current_recipe = raw_recipe  # Delete these two obsolete lines
            current_persons = raw_persons
            recipe_names = [i[0] for i in recipes]
            if raw_recipe in recipe_names:
                index_of_recipe_to_update = recipe_names.index(raw_recipe)
                persons_present = float(persons_raw[index_of_recipe_to_update])
                persons_new = float(raw_persons)
                recipes[index_of_recipe_to_update][1] = persons_present + persons_new
            else:
                recipes.append([raw_recipe, raw_persons])




def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = []
    for raw_recipe, raw_persons in zip(recipes_raw, persons_raw):
        if not raw_recipe == '':  # Simplify conditional expression
            recipe_names = [i[0] for i in recipes]
            if raw_recipe in recipe_names:
                index_of_recipe_to_update = recipe_names.index(raw_recipe)
                persons_present = float(persons_raw[index_of_recipe_to_update])
                persons_new = float(raw_persons)
                recipes[index_of_recipe_to_update][1] = persons_present + persons_new
            else:
                recipes.append([raw_recipe, raw_persons])




def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = []
    for raw_recipe, raw_persons in zip(recipes_raw, persons_raw):
        if raw_recipe:
            recipe_names = [i[0] for i in recipes]  # Give meaningful name to variable
            if raw_recipe in recipe_names:
                index_of_recipe_to_update = recipe_names.index(raw_recipe)
                persons_present = float(persons_raw[index_of_recipe_to_update])
                persons_new = float(raw_persons)
                recipes[index_of_recipe_to_update][1] = persons_present + persons_new
            else:
                recipes.append([raw_recipe, raw_persons])




def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = []
    for raw_recipe, raw_persons in zip(recipes_raw, persons_raw):
        if raw_recipe:
            recipe_names = [recipe[0] for recipe in recipes]
            if raw_recipe in recipe_names:
                index_of_recipe_to_update = recipe_names.index(raw_recipe)
                persons_present = float(persons_raw[index_of_recipe_to_update])
                persons_new = float(raw_persons)  # Eliminate this line
                recipes[index_of_recipe_to_update][1] = persons_present + persons_new
            else:
                recipes.append([raw_recipe, raw_persons])




def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = []  # Use dict instead of list to avoid .index()-call in O(n)
    for raw_recipe, raw_persons in zip(recipes_raw, persons_raw):
        if raw_recipe:
            recipe_names = [recipe[0] for recipe in recipes]
            if raw_recipe in recipe_names:
                index_of_recipe_to_update = recipe_names.index(raw_recipe)
                persons_present = float(persons_raw[index_of_recipe_to_update])
                recipes[index_of_recipe_to_update][1] = persons_present + float(raw_persons)
            else:
                recipes.append([raw_recipe, raw_persons])




def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = {}
    for raw_recipe, raw_persons in zip(recipes_raw, persons_raw):
        if raw_recipe:
            recipe_names = [recipe[0] for recipe in recipes]
            if raw_recipe in recipe_names:
                index_of_recipe_to_update = recipe_names.index(raw_recipe)  # This should be gone by now
                persons_present = float(persons_raw[index_of_recipe_to_update])  # But what's this?
                recipes[raw_recipe] = persons_present + float(raw_persons)
            else:
                recipes[raw_recipe] = raw_persons


# Speculation starting from here...

def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = {}
    for raw_recipe, raw_persons in zip(recipes_raw, persons_raw):
        if raw_recipe:
            if raw_recipe in recipes:
                recipes[raw_recipe] += float(raw_persons)
            else:
                recipes[raw_recipe] = raw_persons
    # Why don't you return anything? How do you even run your code? ^^




def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = {}  # Could use a defaultdict, although not really faster
    for raw_recipe, raw_persons in zip(recipes_raw, persons_raw):
        if raw_recipe:
            if raw_recipe in recipes:
                recipes[raw_recipe] += float(raw_persons)
            else:
                recipes[raw_recipe] = raw_persons
    return recipes




from collections import defaultdict

def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = defaultdict(lambda: 0.0)
    for raw_recipe, raw_persons in zip(recipes_raw, persons_raw):
        if raw_recipe:  # Check this elsewhere
            recipes[raw_recipe] += float(raw_persons)
    return recipes


# hopefully some time later...

from collections import defaultdict

def create_recipe_list_from_form(self, recipes_raw, persons_raw):
    recipes = defaultdict(lambda: 0.0)
    for raw_recipe, raw_persons in zip(recipes_raw, persons_raw):
        recipes[raw_recipe] += float(raw_persons)
    return recipes
